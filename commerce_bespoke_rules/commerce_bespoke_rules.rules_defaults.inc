<?php
/**
 * @file
 * commerce_bespoke_rules.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_bespoke_rules_default_rules_configuration() {
  $items = array();
  $items['rules_maximum_product_quantity_in_cart'] = entity_import('rules_config', '{ "rules_maximum_product_quantity_in_cart" : {
      "LABEL" : "Maximum product quantity in cart",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "commerce_cart" ],
      "ON" : [ "commerce_cart_product_add" ],
      "DO" : [
        { "data_set" : { "data" : [ "commerce-line-item:quantity" ], "value" : "1" } }
      ]
    }
  }');
  $items['rules_redirect_to_cart_when_adding_a_product'] = entity_import('rules_config', '{ "rules_redirect_to_cart_when_adding_a_product" : {
      "LABEL" : "Redirect to cart when adding a product",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "cart", "Commerce" ],
      "REQUIRES" : [ "rules", "commerce_cart" ],
      "ON" : [ "commerce_cart_product_prepare" ],
      "DO" : [ { "redirect" : { "url" : "cart" } } ]
    }
  }');
  return $items;
}
